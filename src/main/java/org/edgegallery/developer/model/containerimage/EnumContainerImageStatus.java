package org.edgegallery.developer.model.containerimage;

public enum EnumContainerImageStatus {
    UPLOAD_WAIT,
    UPLOADING,
    UPLOADING_MERGING,
    UPLOAD_SUCCEED,
    UPLOAD_FAILED,
    UPLOAD_CANCELLED
}
